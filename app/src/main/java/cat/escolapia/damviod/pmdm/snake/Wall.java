package cat.escolapia.damviod.pmdm.snake;

/**
 * Created by Axel on 11/12/2016.
 */

public class Wall {
    public static final int TYPE_1 = 0;
    public WallPart[] walls;

    public Wall(int x, int y, int n) {
        walls = new WallPart[n];
        for (int i=0; i<n;i++)
        {
            WallPart wp = new WallPart(x,y);
            walls[i] = wp;
            y += 1;
        }
    }
    public boolean checkCollision(int x, int y)
    {
        for (int i=0; i<walls.length;i++)
        {
            if(x == walls[i].x && y == walls[i].y ) return true;
        }
        return false;
    }

}
