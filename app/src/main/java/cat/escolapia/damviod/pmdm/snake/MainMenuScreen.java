package cat.escolapia.damviod.pmdm.snake;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class MainMenuScreen extends Screen {
    public MainMenuScreen(Game game) {

        super(game);
        Assets.musicMenu.play();
    }   

    public void update(float deltaTime) {

        Graphics g = game.getGraphics();
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();       
        
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(inBounds(event, 64, 220, 192, 64) ) {
                    game.setScreen(new GameScreen(game));
                    Assets.click.play(1);
                    Assets.musicMenu.stop();
                    return;
                }
                if(inBounds(event, 64, 220 + 88, 192, 64) ) {
                    game.setScreen(new HighscoreScreen(game));
                    Assets.click.play(1);
                    return;
                }
                if(inBounds(event, 64, 308 +88, 192, 64) ) {
                    game.setScreen(new CreditsScreen(game));
                    Assets.click.play(1);
                    return;
                }

            }
        }
    }
    
    private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 && 
           event.y > y && event.y < y + height - 1) 
            return true;
        else
            return false;
    }

    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        
        g.drawPixmap(Assets.background[0], 0, 0);
        g.drawPixmap(Assets.logo, 10, 20);
        g.drawPixmap(Assets.mainMenu, 64, 220);
    }

    public void pause() {
        Settings.save(game.getFileIO());
        Assets.musicMenu.pause();
    }

    public void resume() {
        Assets.musicMenu.resume();
    }

    public void dispose() {

    }
}

